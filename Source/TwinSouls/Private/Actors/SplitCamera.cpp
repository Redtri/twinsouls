// Fill out your copyright notice in the Description page of Project Settings.


#include "SplitCamera.h"

#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "UtilsCustom.h"

// Sets default values
ASplitCamera::ASplitCamera()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetRootComponent());

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->AttachToComponent(SpringArm, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
}

void ASplitCamera::Follow()
{
	//Getting player positions
	if (!Character1 || !Character2) return;
	const FVector Point1 = Character1->GetActorLocation();
	const FVector Point2 = Character2->GetActorLocation();

	//Calculating intersection between both players
	FVector Dir = (Point1 - Point2).GetSafeNormal();
	const FVector Point = Point1 - Dir * (FVector::Distance(Point1, Point2) / 2.f);

	//Dir = Point - GetActorLocation();
	//
	//DrawDebugLine(GetWorld(), Point, Point + FVector::UpVector * 250.f, FColor::Blue);
	//DrawDebugCircle(GetWorld(), Point, 50.f, 50, FColor::Blue, false, -1, 0, 5.f, FVector::UpVector, FVector::RightVector, false);
	//
	//FRotator Rotation = FRotationMatrix::MakeFromX(Dir).Rotator();
	//Rotation = FMath::RInterpTo(GetActorRotation(), Rotation, GetWorld()->GetDeltaSeconds(), Smoothness);

	//const float Dist = FVector::Distance(Point1, Point2);
	//Updating the camera params.
	SetActorLocation(Point);
	//SpringArm->TargetArmLength = Offset.Size(); // Dist - FVector::Distance(Point, GetActorLocation());
	//SetActorRotation(Rotation);
	//
	////Updating player controller rotations considering the main camera. Will make Character move following the camera orientation.
	//static_cast<APlayerController*>(Character1->GetController())->SetControlRotation(Rotation);
	//static_cast<APlayerController*>(Character2->GetController())->SetControlRotation(Rotation);
}

// Called when the game starts or when spawned
void ASplitCamera::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASplitCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Activated) return;
	Follow();
}

void ASplitCamera::SetCharacters(ACharacter* character1, ACharacter* character2)
{
	this->Character1 = character1;
	this->Character2 = character2;
}

