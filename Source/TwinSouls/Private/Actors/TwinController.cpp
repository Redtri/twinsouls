// Fill out your copyright notice in the Description page of Project Settings.


#include "TwinController.h"


#include "RelayPawn.h"
#include "UtilsCustom.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATwinController::ATwinController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATwinController::BeginPlay()
{
	Super::BeginPlay();

	Controller1 = Cast<ARelayController>(GetWorld()->GetFirstPlayerController());
	Controller2 = Cast<ARelayController>(UGameplayStatics::CreatePlayer(GetWorld()));
	SetUpPlayerInputComponent(Controller1->FindComponentByClass<UInputComponent>(), Controller2->FindComponentByClass<UInputComponent>());

    if (SymbioCharBP) {
        SymbioticCharacter = GetWorld()->SpawnActor<ATwinSoulsCharacter>(SymbioCharBP, Spawn1->GetActorLocation(), Spawn1->GetActorRotation());
    }
    if (SplitCharBP) {
		SplitCharacter1 = GetWorld()->SpawnActor<ATwinSoulsCharacter>(SplitCharBP, Spawn1->GetActorLocation(), Spawn1->GetActorRotation());
        SplitCharacter2 = GetWorld()->SpawnActor<ATwinSoulsCharacter>(SplitCharBP, Spawn1->GetActorLocation(), Spawn1->GetActorRotation());

		Cast<ASplitCharacter>(SplitCharacter1)->SetTwinCharacter(SplitCharacter2);
		Cast<ASplitCharacter>(SplitCharacter2)->SetTwinCharacter(SplitCharacter1);
    	
		if (SplitCamera)
			SplitCamera->SetCharacters(SplitCharacter1, SplitCharacter2);
    }
	SymbioticState = false;
	SwitchSymbioticState();
}

void ATwinController::SwitchSymbioticState()
{
	check(Controller1 && Controller2 && SymbioticCharacter && SplitCharacter1 && SplitCharacter2);

	SymbioticState = !SymbioticState;
	
	SymbioticCharacter->SetActorHiddenInGame(!SymbioticState);
	SymbioticCharacter->SetActorEnableCollision(SymbioticState);
	SplitCharacter1->SetActorHiddenInGame(SymbioticState);
	SplitCharacter1->SetActorEnableCollision(!SymbioticState);
	SplitCharacter2->SetActorHiddenInGame(SymbioticState);
	SplitCharacter2->SetActorEnableCollision(!SymbioticState);

	if (SymbioticState)
	{
		SymbioticCharacter->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));
		const auto Rule = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
		SplitCharacter1->AttachToActor(SymbioticCharacter, Rule);
		SplitCharacter2->AttachToActor(SymbioticCharacter, Rule);
		Controller1->SetViewTargetWithBlend(SymbioticCharacter, SplitCamera->BlendingTime);
		if (SplitCamera)
			SplitCamera->Activated = false;
	}
	else
	{
		const auto Rule = FDetachmentTransformRules(EDetachmentRule::KeepWorld, true);
		SplitCharacter1->DetachFromActor(Rule);
		SplitCharacter2->DetachFromActor(Rule);
		SymbioticCharacter->AttachToActor(SplitCharacter1, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
		Controller1->SetViewTargetWithBlend(SplitCamera, SplitCamera->BlendingTime);
		if (SplitCamera)
			SplitCamera->Activated = true;
	}
}

void ATwinController::SwitchState()
{
	SwitchSymbioticState();
}

void ATwinController::SetUpPlayerInputComponent(UInputComponent* PlayerInputComponent1, UInputComponent* PlayerInputComponent2)
{
	check(PlayerInputComponent1);
	check(PlayerInputComponent2);
	
	Cast<ARelayPawn>(Controller1->GetPawn())->SetInputParams(0, this);
	Cast<ARelayPawn>(Controller2->GetPawn())->SetInputParams(1, this);
	
	PlayerInputComponent1->BindAction("SwitchState", IE_Pressed, this, &ATwinController::SwitchState);
}

void ATwinController::Jump(int16 ControllerIndex)
{
	GetCharacter(ControllerIndex)->Jump(ControllerIndex);
}

void ATwinController::StopJumping(int16 ControllerIndex)
{
	GetCharacter(ControllerIndex)->StopJumping(ControllerIndex);
}

void ATwinController::Dash(int16 ControllerIndex)
{
	GetCharacter(ControllerIndex)->Dash(ControllerIndex);
}

void ATwinController::MoveForward(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->MoveForward(ControllerIndex, Value);
	}
}

void ATwinController::MoveRight(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->MoveRight(ControllerIndex, Value);
	}
}

void ATwinController::AddControllerYawInput(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->AddControllerYawInput(ControllerIndex, Value);
	}
}

void ATwinController::TurnAtRate(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->TurnAtRate(ControllerIndex, Value);
	}
}

void ATwinController::AddControllerPitchInput(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->AddControllerPitchInput(ControllerIndex, Value);
	}
}

void ATwinController::LookUpAtRate(int16 ControllerIndex, float Value)
{
	if (FMath::Abs(Value) > AxisDeadZone)
	{
		GetCharacter(ControllerIndex)->LookUpAtRate(ControllerIndex, Value);
	}
}

ATwinSoulsCharacter* ATwinController::GetCharacter(int16 ControllerIndex)
{
	if (SymbioticState)
		return SymbioticCharacter;
	return ControllerIndex == 0 ? SplitCharacter1 : SplitCharacter2;
}

// Called every frame
void ATwinController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
