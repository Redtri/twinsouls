// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityComponent.h"

#include "UtilsCustom.h"

// Sets default values for this component's properties
UAbilityComponent::UAbilityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	IsPerforming = false;
	// ...
}


// Called when the game starts
void UAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	Character = (ACharacter*)(GetOwner());

	if (GetOwner() && !Character) {
		UE_LOG(LogTemp, Warning, TEXT("It seems like you attached an Ability Component on something else than a Character. %s attached to %s won't work properly."), *(GetFName().ToString()), *(GetOwner()->GetFName().ToString()))
	}
	Available = true;
}

void UAbilityComponent::Cooldown()
{
	FTimerDelegate AbilityDelegate{};
	AbilityDelegate.BindUFunction(this, "Coolup");
	GetWorld()->GetTimerManager().SetTimer(CooldownHandle, AbilityDelegate, 5.0f, false, AbilityAsset->CooldownDuration);
	Available = false;
}

void UAbilityComponent::Coolup()
{
	GetWorld()->GetTimerManager().ClearTimer(CooldownHandle);
	Available = true;
}


// Called every frame
void UAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UAbilityComponent::Trigger(uint16 ControllerIndex, float Value)
{
	if (Value == 0.0f)
		return;

	if(Available && !IsPerforming)
	{
		PerformAbility(ControllerIndex);
	}
}

void UAbilityComponent::PerformAbility(uint16 ControllerIndex)
{
	
}

