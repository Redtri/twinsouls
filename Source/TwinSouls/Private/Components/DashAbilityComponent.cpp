// Fill out your copyright notice in the Description page of Project Settings.


#include "DashAbilityComponent.h"


#include "SplitCharacter.h"
#include "UtilsCustom.h"
#include "GameFramework/Character.h"

void UDashAbilityComponent::BeginPlay()
{
	if (!DashData) return;
	
	Super::BeginPlay();
	
	FOnTimelineFloat UpdateTimeline;
	UpdateTimeline.BindUFunction(this, FName("Move"));
	Timeline.AddInterpFloat(DashData->PositionCurve, UpdateTimeline);
	Timeline.SetPlayRate(1.f / DashData->Duration);
	FOnTimelineEventStatic EndTimeline;
	EndTimeline.BindUFunction(this, FName("Cooldown"));
	Timeline.SetTimelineFinishedFunc(EndTimeline);
	Blocked = false;
}

void UDashAbilityComponent::PerformAbility(uint16 ControllerIndex)
{
	SetDashing(true);
	DashDirection = Character->GetActorForwardVector();
	InitialPosition = Character->GetActorLocation();
	Timeline.PlayFromStart();
}

void UDashAbilityComponent::Cooldown()
{
	Super::Cooldown();
	SetDashing(false);
	Blocked = false;
}

void UDashAbilityComponent::Move(float Movement)
{
	const auto SplitChar = Cast<ASplitCharacter>(Character);
	const auto CharPos = SplitChar->GetActorLocation();
	
	if (!Blocked && (SplitChar->GetInterPoint() - CharPos).Size() * 2.f < SplitChar->DeadZoneThreshold)
	{
		const FVector NewPos = InitialPosition + Movement * DashData->Length * DashDirection;
		Character->SetActorLocation(NewPos, true);
	}
	else
	{
		if (!Blocked)
		{
			Blocked = true;
			InitialPosition = CharPos;
		}
		Character->SetActorLocation(InitialPosition, true);
		Character->SetActorRotation(FQuat::FindBetweenNormals(FVector::ForwardVector, DashDirection));
	}
}

void UDashAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (IsPerforming)
		Timeline.TickTimeline(DeltaTime);
}

void UDashAbilityComponent::SetDashing(bool bDashing)
{
	IsPerforming = bDashing;
	OnSetDashing.Broadcast(bDashing);
}
