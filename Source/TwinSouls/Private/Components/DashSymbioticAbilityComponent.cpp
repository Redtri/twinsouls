// Fill out your copyright notice in the Description page of Project Settings.


#include "DashSymbioticAbilityComponent.h"

void UDashSymbioticAbilityComponent::BeginPlay()
{
	if (!DashData) return;

	if (GetAssetAs<USymbioticAbilityAsset>()->SymbiosisWindowDuration >= DashData->SuperDuration)
		UE_LOG(LogTemp, Warning, TEXT("Symbiosis Window should be shorter than Super Dash duration!"));
	
	Super::BeginPlay();

	FOnTimelineFloat UpdateTimelineSimple;
	UpdateTimelineSimple.BindUFunction(this, FName("Move"));
	Timeline.AddInterpFloat(DashData->PositionCurve, UpdateTimelineSimple);
	FOnTimelineEventStatic EndTimeline;
	EndTimeline.BindUFunction(this, FName("Cooldown"));
	Timeline.SetTimelineFinishedFunc(EndTimeline);
}

void UDashSymbioticAbilityComponent::SimpleAbility()
{
	SetDashing(true);
	DashDirection = WantedDashDirection == FVector::ZeroVector || (DashDirection == FVector::ZeroVector && FirstDashWithCharDir) ? Character->GetActorForwardVector() : WantedDashDirection;
	InitialPosition = Character->GetActorLocation();
	Timeline.SetPlayRate(1.f / DashData->Duration);
	Timeline.PlayFromStart();
}

void UDashSymbioticAbilityComponent::SymbiosisAbility()
{
	const float PlaybackPosition = Timeline.GetPlaybackPosition();
	if (PlaybackPosition >= DashData->SuperDuration)
	{
		UE_LOG(LogTemp, Warning, TEXT("Symbiosis Window should be shorter than Super Dash duration!"));
		Timeline.SetNewTime(DashData->SuperDuration - 0.0001f);
	}
	else
	{
		const float RemainingTime = DashData->SuperDuration - PlaybackPosition * DashData->Duration;
		Timeline.SetPlayRate((1.f - PlaybackPosition) / RemainingTime);
	}
	SuperDash = true;
}

void UDashSymbioticAbilityComponent::Cooldown()
{
	Super::Cooldown();
	SuperDash = false;
	WantedDashDirection = FVector::ZeroVector;
	DashDirection = FVector::ZeroVector;
	SetDashing(false);
}

void UDashSymbioticAbilityComponent::Move(float Movement) const
{
	const FVector NewPos = InitialPosition + Movement * (SuperDash ? DashData->SuperLength : DashData->Length) * DashDirection;

	Character->SetActorLocation(NewPos, true);
}

void UDashSymbioticAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	if (IsPerforming)
		Timeline.TickTimeline(DeltaTime);
}

void UDashSymbioticAbilityComponent::SetDashing(bool bDashing)
{
	IsPerforming = bDashing;
	OnSetDashing.Broadcast(bDashing);
}
