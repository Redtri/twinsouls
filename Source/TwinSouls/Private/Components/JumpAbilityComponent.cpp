// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpAbilityComponent.h"

#include "GameFramework/Character.h"

void UJumpAbilityComponent::BeginPlay()
{
    Super::BeginPlay();
	if(Character)
		CharacterMovement = Character->GetCharacterMovement();
}

void UJumpAbilityComponent::PerformAbility(uint16 ControllerIndex)
{
    if (Character)
    {
        UE_LOG(LogTemp, Warning, TEXT("Simple jump"));
        IsPerforming = true;
        CharacterMovement->GravityScale = ((JumpAsset->FinalHeight * 2.f) / FMath::Square(JumpAsset->ReachApexDuration)*0.001f);
        //UE_LOG(LogTemp,Warning,TEXT("G scale: %f"), CharacterMovement->GravityScale);
        CharacterMovement->JumpZVelocity = (2.f * JumpAsset->FinalHeight) / JumpAsset->ReachApexDuration;
        //UE_LOG(LogTemp, Warning, TEXT("V speed: %f"), CharacterMovement->JumpZVelocity);

    	
        Character->Jump();
    }
}

void UJumpAbilityComponent::Coolup()
{
    UE_LOG(LogTemp, Warning, TEXT("Simple jump cooling up"));
    Super::Coolup();
    CharacterMovement->GravityScale = 1.0f;
}

void UJumpAbilityComponent::Landed()
{
	if(Available && IsPerforming)
	{
		IsPerforming = false;
		Cooldown();
	}
}
