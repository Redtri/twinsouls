// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpSymbiosisAbilityComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

void UJumpSymbiosisAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
	if(Character)
	{
		CharacterMovement = Character->GetCharacterMovement();
	}
}

void UJumpSymbiosisAbilityComponent::SimpleAbility()
{
    if(Character)
    {
        UE_LOG(LogTemp, Warning, TEXT("Simple jump"));
		CharacterMovement->GravityScale = ((JumpAsset->FinalHeight * 2.f) / FMath::Square(JumpAsset->ReachApexDuration) * 0.001f);
		//UE_LOG(LogTemp,Warning,TEXT("G scale: %f"), CharacterMovement->GravityScale);
		CharacterMovement->JumpZVelocity = (2.f * JumpAsset->FinalHeight) / JumpAsset->ReachApexDuration;
		//Character->Jump();
    	Character->Jump();
    }
}

void UJumpSymbiosisAbilityComponent::SymbiosisAbility()
{
    if (Character)
    {
		UE_LOG(LogTemp, Warning, TEXT("Super jump"));
		CharacterMovement->GravityScale = ((JumpAsset->SymbioticFinalHeight * 2.f) / FMath::Square(JumpAsset->SymbioticReachApexDuration) * 0.001f);
		//UE_LOG(LogTemp,Warning,TEXT("G scale: %f"), CharacterMovement->GravityScale);
		CharacterMovement->JumpZVelocity = (2.f * JumpAsset->SymbioticFinalHeight) / JumpAsset->SymbioticReachApexDuration;
		Character->Jump();
		//Character->LaunchCharacter(Character->GetControlRotation().Vector().UpVector * JumpAsset->SymbiosisJumpImpulse, false, false);
    }
}

void UJumpSymbiosisAbilityComponent::Trigger(uint16 ControllerIndex, float Value)
{
	if(Character)
	{
		Super::Trigger(ControllerIndex, Value);
	}
}

void UJumpSymbiosisAbilityComponent::Landed()
{
	print("Landed");
	if(Inputs[0] || Inputs[1])
	{
		Cooldown();
	}
}
