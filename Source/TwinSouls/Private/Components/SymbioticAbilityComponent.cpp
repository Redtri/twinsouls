// Fill out your copyright notice in the Description page of Project Settings.


#include "SymbioticAbilityComponent.h"

// Sets default values for this component's properties
USymbioticAbilityComponent::USymbioticAbilityComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;

    Inputs.Add(false);
    Inputs.Add(false);

	Available = true;
	
	Reset();
}


// Called when the game starts
void USymbioticAbilityComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
}

void USymbioticAbilityComponent::Reset(bool ResetInputs)
{
	SymbiosisAvailable = false;
	SymbiosisSuccessful = false;
	if(ResetInputs)
	{
		for (int i = 0; i < Inputs.Num(); ++i)
		{
			Inputs[i] = false;
		}
	}
}

void USymbioticAbilityComponent::Coolup()
{
	GetWorld()->GetTimerManager().ClearTimer(CooldownHandle);
	Reset(true);
	Available = true;
}

//Starting the timer to 
void USymbioticAbilityComponent::OpenSymbiosisWindow(uint16 ControllerIndex)
{
	USymbioticAbilityAsset* SymbioticAsset = GetAssetAs<USymbioticAbilityAsset>();
	if(SymbioticAsset)
	{
		FTimerDelegate AbilityDelegate{};
		AbilityDelegate.BindUFunction(this, "CloseSymbiosisWindow");
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, AbilityDelegate, GetWorld()->GetDeltaSeconds(), false, SymbioticAsset->SymbiosisWindowDuration);

		PerformAbility(ControllerIndex);
		SymbiosisAvailable = true;
	}
}

void USymbioticAbilityComponent::CloseSymbiosisWindow()
{
	SymbiosisAvailable = false;
}

void USymbioticAbilityComponent::PerformAbility(uint16 ControllerIndex)
{
	if(PerformedByTheOther(ControllerIndex))
	{
		SymbiosisSuccessful = SymbiosisAvailable;

		if (SymbiosisSuccessful)
		{
			SymbiosisAbility();
		}else{
			SimpleAbility();
		}

		Reset(false);
	}else
	{
		SimpleAbility();
	}
}

void USymbioticAbilityComponent::SimpleAbility()
{
	
}


void USymbioticAbilityComponent::SymbiosisAbility()
{
	
}

void USymbioticAbilityComponent::Trigger(uint16 ControllerIndex, const float Value)
{
	if (ControllerIndex >= Inputs.Num() || Value == 0.0f)
		return;

	if (Available && !Inputs[ControllerIndex])
	{
		Inputs[ControllerIndex] = true;
		if (!PerformedByTheOther(ControllerIndex))
		{
			//First to trigger this ability.
			OpenSymbiosisWindow(ControllerIndex);
		}
		else {
			//The other controller already performed this ability.
			PerformAbility(ControllerIndex);
		}
	}
}

bool USymbioticAbilityComponent::PerformedByTheOther(uint16 CurrentController)
{
	if(CurrentController < Inputs.Num())
		return Inputs[(CurrentController + 1) % Inputs.Num()];
	return false;
}


// Called every frame
void USymbioticAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

