// Fill out your copyright notice in the Description page of Project Settings.


#include "RelayPawn.h"

#include "TwinController.h"
#include "UtilsCustom.h"

// Sets default values
ARelayPawn::ARelayPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARelayPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARelayPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARelayPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	
	// Set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ARelayPawn::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ARelayPawn::StopJumping);
	PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &ARelayPawn::Dash);

	PlayerInputComponent->BindAxis("MoveForward", this, &ARelayPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARelayPawn::MoveRight);
	//// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	//// "turn" handles devices that provide an absolute delta, such as a mouse.
	//// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &ARelayPawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ARelayPawn::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ARelayPawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ARelayPawn::LookUpAtRate);
}

void ARelayPawn::Jump()
{
	if (TwinController)
		TwinController->Jump(Index);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::StopJumping()
{
	if (TwinController)
		TwinController->StopJumping(Index);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::Dash()
{
	if (TwinController)
		TwinController->Dash(Index);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::MoveForward(float Value)
{
	if (TwinController)
		TwinController->MoveForward(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::MoveRight(float Value)
{
	if (TwinController)
		TwinController->MoveRight(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::AddControllerYawInput(float Value)
{
	if (TwinController)
		TwinController->AddControllerYawInput(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::TurnAtRate(float Value)
{
	if (TwinController)
		TwinController->TurnAtRate(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::AddControllerPitchInput(float Value)
{
	if (TwinController)
		TwinController->AddControllerPitchInput(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

void ARelayPawn::LookUpAtRate(float Value)
{
	if (TwinController)
		TwinController->LookUpAtRate(Index, Value);
	else
		UE_LOG(LogTemp, Warning, TEXT("TwinController is nullptr"));
}

