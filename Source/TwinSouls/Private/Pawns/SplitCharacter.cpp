// Fill out your copyright notice in the Description page of Project Settings.


#include "SplitCharacter.h"

#include "DrawDebugHelpers.h"
#include "UtilsCustom.h"
#include "Components/CapsuleComponent.h"

ASplitCharacter::ASplitCharacter()
{
	JumpAbilityComponent = CreateDefaultSubobject<UJumpAbilityComponent>(TEXT("JumpAbilityComponent"));
	DashAbilityComponent = CreateDefaultSubobject<UDashAbilityComponent>(TEXT("DashAbilityComponent"));
}

void ASplitCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	Movement();
	if(ShowCircle)
	{
		DebugSplitLink();
	}
}

void ASplitCharacter::SetTwinCharacter(ATwinSoulsCharacter* TwinCharacter)
{
	Twin = Cast<ASplitCharacter>(TwinCharacter);
}

void ASplitCharacter::MoveForward(int16 ControllerIndex, float Value)
{
	MovementDirection.Y = Value;
}

void ASplitCharacter::MoveRight(int16 ControllerIndex, float Value)
{
	MovementDirection.X = Value;
}

void ASplitCharacter::JumpEvent_Implementation()
{
}

void ASplitCharacter::Jump(int16 ControllerIndex)
{
	if (Controller && JumpAbilityComponent)
	{
		JumpEvent();
		JumpAbilityComponent->Trigger(ControllerIndex, 1.f);
	}
}

void ASplitCharacter::StopJumping(int16 ControllerIndex)
{
}

void ASplitCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	JumpAbilityComponent->Landed();
}

void ASplitCharacter::Dash(int16 ControllerIndex)
{
	if (Controller && DashAbilityComponent)
	{
		DashAbilityComponent->Trigger(ControllerIndex);
	}
}

FVector ASplitCharacter::GetInterPoint() const
{
	FVector Dir = FVector();
	if (Twin)
	{
		Dir = Twin->GetActorLocation() - GetActorLocation();
	}
	return GetActorLocation() + Dir / 2.0f;
}

bool ASplitCharacter::IsInFreeZone() const
{
	return (GetActorLocation() - Twin->GetActorLocation()).Size() <= MaxFreeRadius;
}

void ASplitCharacter::BeginPlay()
{
	Super::BeginPlay();
	NiagaraComponent = FindComponentByClass<UNiagaraComponent>();
}

void ASplitCharacter::Movement()
{
	float PercentInDeadZone = ((GetActorLocation() - Twin->GetActorLocation()).Size()) / (DeadZoneThreshold);
	if(MovementDirection.Size() > 0.0f)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		
		const FVector Forward = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector Right = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		FVector FinalDirection = (Forward * MovementDirection.Y + Right * MovementDirection.X).GetSafeNormal();
		
		DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + FinalDirection * 500.f, FColor::Blue, false, -1, 0, 2.f);
		if (!IsInFreeZone())
		{
			FVector DirToTwin = (Twin->GetActorLocation() - GetActorLocation()).GetSafeNormal();

			FinalDirection = FMath::Lerp(FinalDirection, DirToTwin, PercentInDeadZone /2.0f);
		}
		AddMovementInput(FinalDirection, 1.0f);


		DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + FinalDirection * 500.f, FColor::Red, false, -1, 0, 2.f);
	}
	if (NiagaraComponent)
	{
		NiagaraComponent->SetFloatParameter(FName("Jitter"), PercentInDeadZone *2.0f);
		NiagaraComponent->SetVectorParameter(FName("Target"), Twin->GetActorLocation());
		NiagaraComponent->SetColorParameter(FName("Color"), FLinearColor::LerpUsingHSV(FColor::White, FColor::Red, PercentInDeadZone *2.0f));
	}
}

void ASplitCharacter::DebugSplitLink()
{
	if(Twin)
	{
		const FVector FeetOffset = -FVector(0.f, 0.f, GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

		FVector CircleCenter = Twin->GetActorLocation()+FeetOffset;
		float FreeRadius = MaxFreeRadius;
		float DeadZoneRadius = DeadZoneThreshold;

		if(CircleOnInterPoint)
		{
			CircleCenter = GetInterPoint()+FeetOffset;
			FreeRadius *= 0.5f;
			DeadZoneRadius *= 0.5f;
		}

		DrawDebugCircle(GetWorld(), CircleCenter, FreeRadius, 100, FColor::Green, false, -1, 0, 2.f, FVector::ForwardVector, FVector::RightVector);
		DrawDebugCircle(GetWorld(), CircleCenter, DeadZoneRadius, 100, FColor::Red, false, -1, 0, 2.f, FVector::ForwardVector, FVector::RightVector);
	}
}
