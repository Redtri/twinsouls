// Fill out your copyright notice in the Description page of Project Settings.


#include "SymbioticCharacter.h"


#include "DrawDebugHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GenericPlatform/GenericPlatformMath.h"

ASymbioticCharacter::ASymbioticCharacter()
{
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	
    DirectionInputs.Add(FVector2D());
    DirectionInputs.Add(FVector2D());
    JumpAbility = CreateDefaultSubobject<UJumpSymbiosisAbilityComponent>(TEXT("JumpAbilityComponent"));
	DashAbility = CreateDefaultSubobject<UDashSymbioticAbilityComponent>(TEXT("DashAbilityComponent"));
}

void ASymbioticCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ASymbioticCharacter::Tick(float DeltaTime)
{
	  Super::Tick(DeltaTime);
	  if(MovementInputChanged)
	  {
			UpdateMovement();
	  }
}

void ASymbioticCharacter::DebugMovement(const FVector Dir1, const FVector Dir2, const FVector FinalDirection, const FVector UpVector) const
{
	//Initializing colors
	FColor ArrowColor = FColor::Black;
	FColor ThresholdColor = FColor::Black;
	//Initializing size values
	float Length = ArrowInfo.Length;//100.f;
	float Thickness = ArrowInfo.Thickness;//2.f;
	float Size = ArrowInfo.Size;//250.f;
	//Setting up references and location calculation
	const UCapsuleComponent* Capsule = GetCapsuleComponent();
	const FVector FeetPosition = GetActorLocation() - FVector(0.f, 0.f, Capsule->GetScaledCapsuleHalfHeight());

	//First controller direction
	ArrowColor = Input1Color;
	DrawDebugDirectionalArrow(GetWorld(), FeetPosition, FeetPosition + Dir1.GetSafeNormal() * Length, Size, ArrowColor, false, -1, 0, Thickness);

	//Second controller direction
	ArrowColor = Input2Color;
	DrawDebugDirectionalArrow(GetWorld(), FeetPosition, FeetPosition + Dir2.GetSafeNormal() * Length, Size, ArrowColor, false, -1, 0, Thickness);

	//Checking for accuracy
	float Angle = FMath::RadiansToDegrees(FGenericPlatformMath::Acos(FVector::DotProduct(Dir1.GetSafeNormal(), Dir2.GetSafeNormal())));
	if (Angle < AccuracyThreshold) {
		ArrowColor = ThresholdColor = AccurateColor;
		Thickness *= 1.25f;
		Size *= 1.25f;
		Length *= 2.f;
	}
	else {
		ArrowColor = ThresholdColor = UnaccurateColor;
	}
	//Threshold
	FVector AnglePoint1 = FinalDirection.RotateAngleAxis(AccuracyThreshold / 2.f, UpVector);
	FVector AnglePoint2 = FinalDirection.RotateAngleAxis(-AccuracyThreshold / 2.f, UpVector);

	DrawDebugLine(GetWorld(), FeetPosition, FeetPosition + AnglePoint1 * Length, ThresholdColor, false, -1, 0, 1.5f);
	DrawDebugLine(GetWorld(), FeetPosition, FeetPosition + AnglePoint2 * Length, ThresholdColor, false, -1, 0, 1.5f);
	DrawDebugLine(GetWorld(), FeetPosition + AnglePoint1, FeetPosition + AnglePoint2, ThresholdColor, false, -1, 0, 1.5f);
	//Final direction
	DrawDebugDirectionalArrow(GetWorld(), FeetPosition, FeetPosition + FinalDirection * Length, Size, ArrowColor, false, -1, 0, Thickness);
	//DrawDebugCone(GetWorld(), FeetPosition, FeetPosition + FinalDirection, ArrowLength, AccuracyThreshold, AccuracyThreshold, 32, ArrowColor, false, -1.f, 0, ArrowWidth);

}

void ASymbioticCharacter::UpdateMovementInputState(bool valid)
{
	if (MovementInputChanged != valid)
		MovementInputChanged = valid;
}

void ASymbioticCharacter::UpdateMovement()
{
	if ((Controller != NULL))
	{
		const FVector Direction1 = ASymbioticCharacter::GetWorldDirection(DirectionInputs[0]);
		//const FVector Direction1 = (ControlRotation.Vector().RightVector/2.f).GetSafeNormal();
		const FVector Direction2 = ASymbioticCharacter::GetWorldDirection(DirectionInputs[1]);
		const FVector FinalDirection = (ComputesMagnitude) ? (Direction1 + Direction2) : (Direction1 + Direction2).GetSafeNormal();

		DebugMovement(Direction1, Direction2, FinalDirection, GetControlRotation().Vector().UpVector);
		AddMovementInput(FinalDirection, FinalDirection.Size());
		UpdateMovementInputState(false);
	}
}

FVector ASymbioticCharacter::GetWorldDirection(const FVector2D Inputs)
{
	FRotator ControlRotation = GetControlRotation();
	return ((ControlRotation.Vector().RightVector * Inputs.X + ControlRotation.Vector().ForwardVector * Inputs.Y)).GetSafeNormal();
}


void ASymbioticCharacter::Jump(int16 ControllerIndex)
{
	if(Controller && JumpAbility)
	{
		//LaunchCharacter(FVector::UpVector * 750, true, true);
		JumpAbility->Trigger(ControllerIndex, 1.f);
	}
}

void ASymbioticCharacter::StopJumping(int16 ControllerIndex)
{
}

void ASymbioticCharacter::Dash(int16 ControllerIndex)
{
	if (Controller && DashAbility)
	{
		DashAbility->SetWantedDashDirection(ASymbioticCharacter::GetWorldDirection(DirectionInputs[ControllerIndex]));
		DashAbility->Trigger(ControllerIndex, 1.f);
	}
}

void ASymbioticCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	JumpAbility->Landed();
}

void ASymbioticCharacter::MoveForward(int16 ControllerIndex, float Value)
{
	if (!Controller || ControllerIndex >= DirectionInputs.Num())
		return;

	DirectionInputs[ControllerIndex].Y = Value;
	DirectionInputs[ControllerIndex].Normalize();
	
	if(Value != 0.0f)
	{
		UpdateMovementInputState(true);
	}
}

void ASymbioticCharacter::MoveRight(int16 ControllerIndex, float Value)
{
	if (!Controller || ControllerIndex >= DirectionInputs.Num())
		return;

	DirectionInputs[ControllerIndex].X = Value;
	DirectionInputs[ControllerIndex].Normalize();

	if (Value != 0.0f)
	{
		UpdateMovementInputState(true);
	}
}

void ASymbioticCharacter::TurnAtRate(int16 ControllerIndex, float Rate)
{
}

void ASymbioticCharacter::LookUpAtRate(int16 ControllerIndex, float Rate)
{
}

void ASymbioticCharacter::AddControllerYawInput(float Value)
{
}

void ASymbioticCharacter::AddControllerYawInput(int16 ControllerIndex, float Value)
{
}

void ASymbioticCharacter::AddControllerPitchInput(float Value)
{
}

void ASymbioticCharacter::AddControllerPitchInput(int16 ControllerIndex, float Value)
{
}
