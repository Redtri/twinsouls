// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "SplitCamera.generated.h"

UCLASS(Blueprintable)
class TWINSOULS_API ASplitCamera : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		bool Activated;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Customizable")
		float BlendingTime;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Customizable")
		float Smoothness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Customizable")
		FVector Offset;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere)
		ACharacter* Character1;
	UPROPERTY(VisibleAnywhere)
		ACharacter* Character2;

public:
	// Sets default values for this actor's properties
	ASplitCamera();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetCharacters(ACharacter* Character1, ACharacter* Character2);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void Follow();
};
