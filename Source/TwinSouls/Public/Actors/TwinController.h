// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RelayController.h"
#include "TwinSouls/TwinSoulsCharacter.h"
#include "SplitCharacter.h"
#include "SplitCamera.h"

#include "TwinController.generated.h"

UCLASS()
class TWINSOULS_API ATwinController : public AActor
{
	GENERATED_BODY()

	DECLARE_DELEGATE_OneParam(FControllerDelegate, ARelayController*);

	UPROPERTY()
		ARelayController* Controller1;
	UPROPERTY()
		ARelayController* Controller2;
	UPROPERTY(EditAnywhere, meta = (ClampMin = "-0.01", ClampMax = "0.5"))
		float AxisDeadZone = -0.01f;

		bool SymbioticState;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ATwinSoulsCharacter> SymbioCharBP;
	UPROPERTY()
		ATwinSoulsCharacter* SymbioticCharacter;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ATwinSoulsCharacter> SplitCharBP;
	UPROPERTY()
		ATwinSoulsCharacter* SplitCharacter1;
	UPROPERTY()
		ATwinSoulsCharacter* SplitCharacter2;
	UPROPERTY(EditAnywhere)
		AActor* Spawn1;
	UPROPERTY(EditAnywhere)
		ASplitCamera* SplitCamera;
	
public:	
	// Sets default values for this actor's properties
	ATwinController();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FVector GetInterBetweenSplits();
	
	// Inputs
	virtual void SetUpPlayerInputComponent(class UInputComponent* PlayerInputComponent1, UInputComponent* PlayerInputComponent2);
	void Jump(int16 ControllerIndex);
	void StopJumping(int16 ControllerIndex);
	void Dash(int16 ControllerIndex);

	void MoveForward(int16 ControllerIndex, float Value);
	void MoveRight(int16 ControllerIndex, float Value);
	void AddControllerYawInput(int16 ControllerIndex, float Value);
	void TurnAtRate(int16 ControllerIndex, float Value);
	void AddControllerPitchInput(int16 ControllerIndex, float Value);
	void LookUpAtRate(int16 ControllerIndex, float Value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SwitchSymbioticState();
	// TODO suppr
	void SwitchState();

private:
	ATwinSoulsCharacter* GetCharacter(int16 ControllerIndex);
	void DebugSplitLink();
};
