// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AbilityAsset.h"

#include "AbilityComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TWINSOULS_API UAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ACharacter* Character;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	UAbilityAsset* AbilityAsset;
	
	bool Available;
	bool IsPerforming;
	FTimerHandle TimerHandle;
	FTimerHandle CooldownHandle;
	
public:	
	// Sets default values for this component's properties
	UAbilityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void Cooldown();
	UFUNCTION()
	virtual void Coolup();
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void Trigger(uint16 ControllerIndex, float Value = 1.0f);
	virtual void PerformAbility(uint16 ControllerIndex);

	virtual bool CurrentlyPerforming() const { return IsPerforming; }
	virtual bool CanBePerformed() const { return Available; }
	virtual void AllowPerformance() { Available = true; }

	template<typename T>
	T* GetAssetAs();
};

template <typename T>
T* UAbilityComponent::GetAssetAs()
{
	return (Cast<T>(AbilityAsset));
}
