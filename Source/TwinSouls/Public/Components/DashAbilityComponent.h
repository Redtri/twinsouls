// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/AbilityComponent.h"
#include "DashAbilityAsset.h"
#include "Components/TimelineComponent.h"


#include "DashAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetDashing, bool, bDashing);

/**
 * 
 */
UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TWINSOULS_API UDashAbilityComponent : public UAbilityComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UDashAbilityAsset* DashData;
	
private:
	FVector DashDirection;
	FVector InitialPosition;
	FTimeline Timeline;
	bool Blocked;


protected:
	virtual void BeginPlay() override;

	virtual void PerformAbility(uint16 ControllerIndex) override;

	UFUNCTION()
	virtual void Cooldown() override;
	UFUNCTION()
	void Move(float Movement);

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(BlueprintAssignable, Category = "Ability")
		FOnSetDashing OnSetDashing;
	void SetDashing(bool bDashing);
};
