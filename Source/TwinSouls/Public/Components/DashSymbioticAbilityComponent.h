// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SymbioticAbilityComponent.h"
#include "SymbioticDashAbilityAsset.h"
#include "Components/TimelineComponent.h"

#include "DashSymbioticAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetDashingSymbio, bool, bDashing);

/**
 * 
 */
UCLASS()
class TWINSOULS_API UDashSymbioticAbilityComponent : public USymbioticAbilityComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		USymbioticDashAbilityAsset* DashData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool FirstDashWithCharDir;

private:
	FVector WantedDashDirection;
	FVector DashDirection;
	FVector InitialPosition;
	FTimeline Timeline;
	bool SuperDash;
	
public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Ability")
		FOnSetDashingSymbio OnSetDashing;
	void SetDashing(bool bDashing);

	void SetWantedDashDirection(const FVector wantedDashDirection) { WantedDashDirection = wantedDashDirection; }
	
protected:
	virtual void BeginPlay() override;
	virtual void SimpleAbility() override;
	virtual void SymbiosisAbility() override;

	UFUNCTION()
		virtual void Cooldown() override;
	UFUNCTION()
		void Move(float Movement) const;
};
