// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityComponent.h"
#include "JumpAbilityAsset.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UtilsCustom.h"
#include "DrawDebugHelpers.h"

#include "JumpAbilityComponent.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API UJumpAbilityComponent : public UAbilityComponent
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UJumpAbilityAsset* JumpAsset;

	UPROPERTY()
	UCharacterMovementComponent* CharacterMovement;
	float InitialGravityScale;
	FVector InitialVelocity;
	
protected:
	virtual void BeginPlay() override;
	
	virtual void PerformAbility(uint16 ControllerIndex) override;
	virtual void Coolup() override;
public:
	virtual void Landed();
};
