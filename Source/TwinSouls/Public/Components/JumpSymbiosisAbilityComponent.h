// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SymbioticAbilityComponent.h"

#include "SymbioticJumpAbilityAsset.h"

#include "JumpSymbiosisAbilityComponent.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API UJumpSymbiosisAbilityComponent : public USymbioticAbilityComponent
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	USymbioticJumpAbilityAsset* JumpAsset;

	UPROPERTY()
	UCharacterMovementComponent* CharacterMovement;
protected:
	virtual void BeginPlay() override;
	virtual void SimpleAbility() override;
	virtual void SymbiosisAbility() override;
public:
	virtual void Trigger(uint16 ControllerIndex, float Value) override;
	virtual void Landed();
};
