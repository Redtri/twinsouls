// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Character.h"
#include "UtilsCustom.h"
#include "AbilityComponent.h"
#include "SymbioticAbilityAsset.h"

#include "SymbioticAbilityComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TWINSOULS_API USymbioticAbilityComponent : public UAbilityComponent
{
	GENERATED_BODY()
protected:
	
    TArray<bool> Inputs;
public:
    bool SymbiosisAvailable;
    bool SymbiosisSuccessful;

public:	
	// Sets default values for this component's properties
      USymbioticAbilityComponent();
      // Called every frame
      virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	
protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    virtual void Reset(bool ResetInputs = true);

    virtual void Coolup() override;
	
    virtual void OpenSymbiosisWindow(uint16 ControllerIndex);
    UFUNCTION()
    virtual void CloseSymbiosisWindow();
    virtual void PerformAbility(uint16 ControllerIndex) override;
	
    virtual void SimpleAbility();
    virtual void SymbiosisAbility();

public:
    virtual void Trigger(uint16 ControllerIndex, float Value = 1.0f) override;
    virtual bool PerformedByTheOther(uint16 CurrentController = 0);
};
