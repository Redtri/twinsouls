// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "AbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API UAbilityAsset : public UDataAsset
{
	GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Durations")
    float CooldownDuration;
};
