// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DashAbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API UDashAbilityAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UCurveFloat* PositionCurve;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "100", ClampMax = "2000"))
		float Length;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.01", ClampMax = "0.5"))
		float Duration;
};
