// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "JumpAbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API UJumpAbilityAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseImpulseForce;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FinalHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReachApexDuration;
};
