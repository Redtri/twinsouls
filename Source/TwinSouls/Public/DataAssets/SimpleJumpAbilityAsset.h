// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityAsset.h"
#include "JumpAbilityAsset.h"

#include "SimpleJumpAbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API USimpleJumpAbilityAsset : public UAbilityAsset
{
      GENERATED_BODY()
public:
          UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
          float SimpleJumpImpulse = 0.0f;
};
