// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityAsset.h"
#include "SymbioticAbilityAsset.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TWINSOULS_API USymbioticAbilityAsset : public UAbilityAsset
{
	GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Durations")
    float SymbiosisWindowDuration;
};
