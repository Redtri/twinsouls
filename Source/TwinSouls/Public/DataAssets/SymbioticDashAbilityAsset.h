// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DataAssets/DashAbilityAsset.h"
#include "SymbioticDashAbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API USymbioticDashAbilityAsset : public UDashAbilityAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Super Dash", meta = (ClampMin = "100", ClampMax = "2000"))
		float SuperLength;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Super Dash", meta = (ClampMin = "0.01", ClampMax = "0.5"))
		float SuperDuration;
	
};
