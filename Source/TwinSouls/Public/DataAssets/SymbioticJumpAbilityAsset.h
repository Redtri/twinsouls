// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "JumpAbilityAsset.h"
#include "SymbioticJumpAbilityAsset.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API USymbioticJumpAbilityAsset : public UJumpAbilityAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SymbioticFinalHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SymbioticReachApexDuration;
};
