// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#define printFString(text, fstring) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT(text), fstring))
#define printFStrings(text, fstring, fstring2) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT(text), fstring, fstring2))
#define printFVector(vector) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("%s"), *(vector.ToString())))

/**
 * 
 */
static class TWINSOULS_API UtilsCustom
{
public:
	/**
	 * Not sure it is working
	 * @param O Line origin
	 * @param D Line direction
	 * @param C Circle center
	 * @param radius Circle radius
	 */
	static bool IntersectLineCircle(const FVector2D& O, const FVector2D& D, const FVector2D& C, const float radius)
	{
		const FVector2D CO = O - C;
		const float a = FVector2D::DotProduct(D, D);
		const float b = FVector2D::DotProduct(CO, D);
		const float c = FVector2D::DotProduct(CO, CO) - radius * radius;
		const float det = b * b - a * c;
		printFString("%f", det);
		if (det < 0.f) {
			return false;
		}
		return true;
	}
};
