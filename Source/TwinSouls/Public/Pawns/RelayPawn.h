// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "RelayPawn.generated.h"

class ATwinController;
UCLASS()
class TWINSOULS_API ARelayPawn : public APawn
{
	GENERATED_BODY()

private:
	int16 Index;
	UPROPERTY()
	ATwinController* TwinController;
	
public:
	// Sets default values for this pawn's properties
	ARelayPawn();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetInputParams(int16 index, ATwinController* twinController) { Index = index; TwinController = twinController; }


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent);
	
	// Inputs
	void Jump();
	void StopJumping();
	void Dash();

	void MoveForward(float Value);
	void MoveRight(float Value);
	void AddControllerYawInput(float Value);
	void TurnAtRate(float Value);
	void AddControllerPitchInput(float Value);
	void LookUpAtRate(float Value);
};
