// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TwinSouls/TwinSoulsCharacter.h"
#include "JumpAbilityComponent.h"
#include "NiagaraComponent.h"
#include "DashAbilityComponent.h"

#include "SplitCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TWINSOULS_API ASplitCharacter : public ATwinSoulsCharacter
{
	GENERATED_BODY()
    
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxFreeRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DeadZoneThreshold;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UJumpAbilityComponent* JumpAbilityComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UDashAbilityComponent* DashAbilityComponent;
	UPROPERTY(VisibleAnywhere)
	UNiagaraComponent* NiagaraComponent;
	
	UPROPERTY(VisibleAnywhere)
	ASplitCharacter* Twin;
	FVector MovementDirection;

	UPROPERTY(EditAnywhere, Category = "Debug|SplitLink")
		bool ShowCircle;
	UPROPERTY(EditAnywhere, Category = "Debug|SplitLink")
		bool CircleOnInterPoint;
    
public:
	ASplitCharacter();
	virtual void Tick(float DeltaSeconds) override;

	//Initialization
	void SetTwinCharacter(ATwinSoulsCharacter* TwinCharacter);
	
	//Abilities
	void MoveForward(int16 ControllerIndex, float Value) override;
	/** Called for side to side input */
	void MoveRight(int16 ControllerIndex, float Value) override;
	UFUNCTION(BlueprintNativeEvent)
	void JumpEvent();
	void JumpEvent_Implementation();
	virtual void Jump(int16 ControllerIndex) override;
	virtual void StopJumping(int16 ControllerIndex);
	virtual void Landed(const FHitResult& Hit) override;

	virtual void Dash(int16 ControllerIndex) override;
	
	FVector GetInterPoint() const;
	bool IsInFreeZone() const;
protected:
	virtual void BeginPlay() override;
private:
	void Movement();
	void DebugSplitLink();
};
