// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TwinSouls/TwinSoulsCharacter.h"
#include "JumpSymbiosisAbilityComponent.h"
#include "DashSymbioticAbilityComponent.h"

#include "SymbioticCharacter.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FArrowInfo
{
	GENERATED_USTRUCT_BODY()
	FColor Color;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Length;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Thickness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Size;
};

UCLASS()
class TWINSOULS_API ASymbioticCharacter : public ATwinSoulsCharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

protected:
	//Movement
	UPROPERTY(EditAnywhere, Category = "Balancing|Movement")
		float AccuracyThreshold;
	UPROPERTY(EditAnywhere, Category = "Balancing|Movement")
		bool ComputesMagnitude;

	//Jump
	UPROPERTY(EditAnywhere, Category = "Balancing|Abilities")
		UJumpSymbiosisAbilityComponent* JumpAbility;

	// Dash
	UPROPERTY(EditAnywhere, Category = "Balancing|Abilities")
		UDashSymbioticAbilityComponent* DashAbility;

	UPROPERTY(EditAnywhere, Category = "Debug|General")
		FArrowInfo ArrowInfo;
	UPROPERTY(EditAnywhere, Category = "Debug|Input1")
		FColor Input1Color;
	UPROPERTY(EditAnywhere, Category = "Debug|Input2")
		FColor Input2Color;
	UPROPERTY(EditAnywhere, Category = "Debug|FinalInput")
		FColor UnaccurateColor;
	UPROPERTY(EditAnywhere, Category = "Debug|FinalInput")
		FColor AccurateColor;

	TArray<FVector2D> DirectionInputs;
	bool MovementInputChanged;

	//METHODS

public:
	ASymbioticCharacter();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
protected:
	virtual void UpdateMovementInputState(bool valid);
	virtual void UpdateMovement();
	virtual FVector GetWorldDirection(const FVector2D Inputs);
	
private:
	void DebugMovement(const FVector Dir1, const FVector Dir2, const FVector FinalDirection, const FVector UpVector) const;
	
public:
	
	virtual void Jump(int16 ControllerIndex) override;
	virtual void StopJumping(int16 ControllerIndex);

	virtual void Dash(int16 ControllerIndex) override;

	virtual void Landed(const FHitResult& Hit) override;

	/** Called for forwards/backward input */
	virtual void MoveForward(int16 ControllerIndex, float Value);

	/** Called for side to side input */
	virtual void MoveRight(int16 ControllerIndex, float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	virtual void TurnAtRate(int16 ControllerIndex, float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	virtual void LookUpAtRate(int16 ControllerIndex, float Rate);

	virtual void AddControllerYawInput(float Value) override;
	virtual void AddControllerYawInput(int16 ControllerIndex, float Value);

	virtual void AddControllerPitchInput(float Value) override;
	virtual void AddControllerPitchInput(int16 ControllerIndex, float Value);


	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};
