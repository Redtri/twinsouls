// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TwinSouls : ModuleRules
{
	public TwinSouls(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Niagara" });

		string BasePublicPath = "TwinSouls/Public";
		string BasePrivatePath = "TwinSouls/Private";

		PublicIncludePaths.AddRange(new string[] {	BasePublicPath + "/DataAssets",
													BasePublicPath + "/Components",
													BasePublicPath + "/Controllers" ,
													BasePublicPath + "/Other",
													BasePublicPath + "/Pawns",
													BasePublicPath + "/Actors",
												});

		PrivateIncludePaths.AddRange(new string[] {	BasePrivatePath + "/DataAssets",
													BasePrivatePath + "/Components",
													BasePrivatePath + "/Controllers" ,
													BasePrivatePath + "/Other",
													BasePrivatePath + "/Pawns",
													BasePrivatePath + "/Actors"
												});
	}
}
