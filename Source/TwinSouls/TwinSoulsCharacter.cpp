// Copyright Epic Games, Inc. All Rights Reserved.

#include "TwinSoulsCharacter.h"

#include "UtilsCustom.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"

//////////////////////////////////////////////////////////////////////////
// ATwinSoulsCharacter

ATwinSoulsCharacter::ATwinSoulsCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATwinSoulsCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
}

void ATwinSoulsCharacter::TurnAtRate(int16 ControllerIndex, float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(ControllerIndex, Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATwinSoulsCharacter::LookUpAtRate(int16 ControllerIndex, float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(ControllerIndex, Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATwinSoulsCharacter::AddControllerYawInput(float Value) {
	Super::AddControllerYawInput(Value);
}

void ATwinSoulsCharacter::AddControllerYawInput(int16 ControllerIndex, float Value)
{
}

void ATwinSoulsCharacter::AddControllerPitchInput(float Value) {
	Super::AddControllerPitchInput(Value);
}

void ATwinSoulsCharacter::AddControllerPitchInput(int16 ControllerIndex, float Value)
{
}

void ATwinSoulsCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Spawning a controller associated with the character. Ensures that the character can rely on a controller for its movements.
	Controller = GetWorld()->SpawnActor<APlayerController>(APlayerController::StaticClass(), FVector(), FRotator());
	if (Controller)
		Controller->Possess(this);
	else
		UE_LOG(LogTemp, Warning, TEXT("Couldn't reference the spawn controller"));
}

void ATwinSoulsCharacter::Jump() {
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, "Calling super");
	ACharacter::Jump();
}

void ATwinSoulsCharacter::Jump(int16 ControllerIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Jump not overriden"));
	Jump();
}

void ATwinSoulsCharacter::StopJumping() {
	Super::StopJumping();
}

void ATwinSoulsCharacter::StopJumping(int16 ControllerIndex)
{
	StopJumping();
}

void ATwinSoulsCharacter::Dash(int16 ControllerIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Dash not overriden"));
}

void ATwinSoulsCharacter::MoveForward(int16 ControllerIndex, float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATwinSoulsCharacter::MoveRight(int16 ControllerIndex, float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
