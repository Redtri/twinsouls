// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TwinSoulsCharacter.generated.h"

UCLASS(config=Game)
class ATwinSoulsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATwinSoulsCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	
public:
	virtual void BeginPlay() override;
	
	virtual void Jump() override;
	virtual void Jump(int16 ControllerIndex);
	virtual void StopJumping() override;
	virtual void StopJumping(int16 ControllerIndex);
	virtual void Dash(int16 ControllerIndex);

	/** Called for forwards/backward input */
	virtual void MoveForward(int16 ControllerIndex, float Value);

	/** Called for side to side input */
	virtual void MoveRight(int16 ControllerIndex, float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	virtual void TurnAtRate(int16 ControllerIndex, float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	virtual void LookUpAtRate(int16 ControllerIndex, float Rate);

	virtual void AddControllerYawInput(float Value) override;
	virtual void AddControllerYawInput(int16 ControllerIndex, float Value);
	
	virtual void AddControllerPitchInput(float Value) override;
	virtual void AddControllerPitchInput(int16 ControllerIndex, float Value);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
};

