// Copyright Epic Games, Inc. All Rights Reserved.

#include "TwinSoulsGameMode.h"
#include "TwinSoulsCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATwinSoulsGameMode::ATwinSoulsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
