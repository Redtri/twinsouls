// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TwinSoulsGameMode.generated.h"

UCLASS(minimalapi)
class ATwinSoulsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATwinSoulsGameMode();
};



